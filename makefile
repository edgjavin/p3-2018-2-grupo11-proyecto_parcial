programa:	estatico dinamico

estatico:	obj/main.o libs/liblista.a
	gcc -llista -static -Llibs obj/main.o libs/liblista.a -o bin/$@ -g

dinamico:	obj/main.o libs/liblista.so
	gcc -llista -Llibs obj/main.o libs/liblista.so -o bin/$@ -g

libs/liblista.a:	obj/lista.o
	ar rcs $@ $<

obj/lista.o:	src/lista.c
	gcc -Wall -I include/ -fPIC $< -o $@


libs/liblista.so:	src/lista.c
	gcc -Wall -I include/ -fPIC -shared $< -o $@

obj/main.o:	src/main.c
	gcc -Wall -I include/ -fPIC $< -o $@

