#ifndef _LISTA_H
#define _LISTA_H
#include <stdlib.h>
/**
* Autor: Eduardo Murillo, M.Sc..
*/


/*Algunas definiciones*/
#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/**
* Definci�n del elemento de la lista
*/

typedef struct ElementoListaTDA{
	
	//Definir los campos del elemento de lista
} ElementoLista;
/**
*Definci�n de la estructura de lista
*/

typedef struct ListaEnlazadaTDA{
}ListaEnlazada;

/*Definici�n de funciones sobre la lista*/
extern int Lista_Inicializar(ListaEnlazadaTDA lista);		//0 en exito, -1 en error

extern int Lista_Vacia(/* �arguementos? */);			//TRUE si esta vacia, FALSE si no
extern int Lista_Conteo(/* �arguementos? */);			//Cantidad de elementos en la lista

extern ElementoLista *Lista_Buscar(/* �arguementos? */);

/*Funciones de modificacion de la lista*/
extern int Lista_InsertarFin(/* �arguementos? */);			//0 en exito, -1 en error

extern int Lista_InsertarInicio();		//0 en exito, -1 en error
extern void Lista_Sacar(/* �arguementos? */);				//saca un elemento de la lista
extern void Lista_SacarTodos(/* �arguementos? */);
extern int Lista_InsertarDespues(/* �arguementos? */);	//0 en exito, -1 en error. Inserta un objeto despues de un elemento particular de la lista
extern int Lista_InsertarAntes(/* �arguementos? */);	//0 en exito, -1 en error. Inserta un objeto antes de un elemento particular de la lista


/*Funciones que devuelven un elemento particular de la lista*/
extern ElementoLista *Lista_Primero(/* �arguementos? */);		//NULL en error
extern ElementoLista *Lista_Ultimo(/* �arguementos? */);		//NULL en error
extern ElementoLista *Lista_Siguiente(/* �arguementos? */);	//NULL en error. Obtiene elemento despues de un elemento particular
extern ElementoLista *Lista_Anterior(/* �arguementos? */);	//NULL en error. Obtiene elemento antes de un elemento particular


#endif
